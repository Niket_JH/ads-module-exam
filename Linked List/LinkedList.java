class LinkedList
{

	Node head ;
	
public static class Node
{
	int data;
	Node next;
	
	
	Node (int d)
	{
	data = d;
	next = null;
	}
}	
	
	public void display()
	{
		
		
		Node n = head;
		
		if(n!=null)
		{
			System.out.println(n.data);
			n=n.next;
		}
	}
	
	public static void main (String [] args)
	{
		LinkedList l = new LinkedList();
		l.head = new Node(5);
		Node second = new Node(10);
Node third = new Node (15);
		
		l.head.next = second;
		
		
		second.next = third;
		
		
		l.display();
		
		
		
	}
	
}